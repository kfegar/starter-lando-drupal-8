#!/bin/bash

cat << "EOF"
+----------------------------------------+
   __   ___   _  _____  ____
  / /  / _ | / |/ / _ \/ __ \
 / /__/ __ |/    / // / /_/ /
/____/_/ |_/_/|_/____/\____/

Local Build for Drupal 8 !

+----------------------------------------+
EOF

# Start
start=$(date +"%s")

# Build commands.
composer install

drush cache-rebuild
drush config-import -n
drush config-split:import -y
drush updatedb

# drush entity-updates -l lamberet -y

echo "+----------------------------------------+"
end=$(date +"%s")
difftimelps=$(($end-$start))
echo "$(($difftimelps / 60)) minutes $(($difftimelps % 60)) seconds elapsed time."
echo "+----------------------------------------+"
