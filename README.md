## Installation

This installation guide already assumes that you have both Docker and Lando installed. To install both of these please Visit:
*  https://docs.lando.dev/basics/installation.html

### Download the repo
First off your going to need to download the repository localy. Open up your terminal and head to whichever folder you would like the project to be created.
```
git clone https://framagit.org/kfegar/starter-lando-drupal-8.git drupal_project
cd drupal_project
```
When you successfully clone the project, you do not need to keep this repo active.
```
git remote remove origin
```
### Turn on lando
You might want to edit the .lando.yml in the root directory and change the name as this is what your local url will be based off of.

Now lets get your site running locally on lando.
```
lando start
```

Done. to see all lando commands just type and enter the below into the terminal
```
lando
```

### Install dependencies
Now install Drupal

To install Drupal core, modules and any other php dependencies run
```
cd www
composer install
```

### Visit your site and setup the db
Final Step!! Now you just need to go on your website locally on the site url that lando has created for you and setup your Drupal credentials and profile settings

## Fini!!!